<?php
require ('animal.php');


$sheep = new Animal("shaun", 4);

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"

$sungokong = new Ape("kera sakti", 2);
echo "Name : " . $sungokong->name . "<br>"; // "shaun"
echo "Legs : " . $sungokong->legs . "<br>"; // 4
echo "Cold Blooded : " . $sungokong->cold_blooded; // "no"
echo "Yell : ";
$sungokong->yell();// "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk", 4);
echo "Name : " . $kodok->name . "<br>"; // "shaun"
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "Cold Blooded : " . $kodok->cold_blooded; // "no"
echo "Jump : ";
$kodok->jump(); // "hop hop"

?>